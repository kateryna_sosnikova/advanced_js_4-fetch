const button = document.createElement('button');
document.body.append(button);
button.innerText = 'Вычислить по IP';
button.onclick = getIp;

const info = document.createElement('p');
document.body.append(info);
const country = document.createElement('p');
document.body.append(country);
const region = document.createElement('p');
document.body.append(region);
const city = document.createElement('p');
document.body.append(city);
const zip = document.createElement('p');
document.body.append(zip);

function getIp() {

    const request = fetch('https://api.ipify.org/?format=json');

    request
        .then(response => {
            if (!response.ok) throw new Error('Страница на api.ipify.org не найдена');
            return response.json();
        })
        .then(data => {
            console.log(data)
            const request2 = fetch('http://ip-api.com/json/'+data.ip);

            request2
                .then(response => {
                    if (!response.ok) throw new Error('Страница на ip-api.com не найдена');
                    return response.json();
                })
                .then((result) => {
                    info.innerHTML = 'Your location information:';
                    country.innerHTML = result.country;
                    region.innerHTML = result.regionName;
                    city.innerHTML = result.city;
                    zip.innerHTML = result.zip;
                })
                .catch((error) => {
                    console.log(error);
                })
        })
   
}